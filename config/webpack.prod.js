/**
 * Webpack production config
 */

const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.js')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
// const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = merge(base, {
    mode: 'production',
    output: {
        publicPath: 'www.lichangxin.net/static/', // Online static directory
        filename: '[name].[hash].js',
        chunkFilename: '[name].[chunkhash].js',
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].[hash].css'
        }),
        new UglifyJsPlugin({
            parallel: true,
            uglifyOptions: {
                ecma: 8,
                output: {
                    comments: false,
                    beautify: false
                }
            }
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../src/tpl/index.tpl')
        }),
        new webpack.HashedModuleIdsPlugin()
    ]
})